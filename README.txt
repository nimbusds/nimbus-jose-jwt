THIS GIT REPO HAS BEEN MOVED TO THE CONNECT2ID TEAM


The Nimbus library for JavaScript Object Signing and Encryption (JOSE) and JSON
Web Tokens (JWT) has been moved to 

https://bitbucket.org/connect2id/nimbus-jose-jwt

Please, update your bookmarks and git links accordingly.


2014-04-02
